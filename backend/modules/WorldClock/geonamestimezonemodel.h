/*
 * Copyright (C) 2022 Guido Berhoerster <guido+ubports@berhoerster.name>
 *
 * This file is part of Lomiri Clock App
 *
 * Clock App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Clock App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEONAMESTIMEZONEMODEL_H
#define GEONAMESTIMEZONEMODEL_H

#include <gio/gio.h>

#include "timezonemodel.h"

class GeonamesTimeZoneModel : public TimeZoneModel
{
    Q_OBJECT

    // Property to store the query
    Q_PROPERTY(QString query
               READ query
               WRITE setQuery
               NOTIFY queryChanged)

public:
    GeonamesTimeZoneModel(QObject *parent = 0);

    // Function to read the query
    QString query() const;

    // Function to set the query
    void setQuery(const QString &query);

Q_SIGNALS:
    // Signal to notify the change of the query to QML
    void queryChanged();

private:
    // Private copy of the sourcy received from QML
    QString m_query;

    // glib Callback when the query is finished
    static void queryFinished(GObject *source_object, GAsyncResult *result, gpointer user_data);

    // Function to update the model after retrieving a query result
    void updateModel(gint *cities, guint len);

    // Function to initiate the retrieval process
    void loadTimeZonesFromGeonames();
};

#endif // GEONAMESTIMEZONEMODEL_H
