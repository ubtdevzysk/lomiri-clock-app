/*
 * Copyright (C) 2014-2016 Canonical Ltd.
 *
 * This file is part of Lomiri Clock App
 *
 * Lomiri Clock App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Lomiri Clock App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4

QtObject {
    
    function getUrlQueryArguments(url) {
        var matches = url.match(/([\w\d_]+=[^$&]+)/g);
        var query = {};
        
        for(var i in matches) {
            var key = matches[i].match(/([\w\d_]+)=/)
            var val = matches[i].match(/=(.*)/)
            query[key[1]] = val[1];
        }

        return query;
    }
}
